# vim:set ft=sh:
# Maintainer: BlackEagle < ike DOT devolder AT gmail DOT com >
# Contributor: Tobias Powalowski <tpowa@archlinux.org>
# Contributor: Thomas Baechler <thomas@archlinux.org>

_kernelname=-besrv-lts
pkgbase="linux$_kernelname"
pkgname=("linux$_kernelname" "linux$_kernelname-headers")
_basekernel=6.12
_patchver=18
if [[ $_patchver -ne 0 ]]; then
    _tag=v${_basekernel}.${_patchver}
    pkgver=${_basekernel}.${_patchver}
else
    _tag=v${_basekernel}
    pkgver=${_basekernel}
fi
pkgrel=1
arch=('x86_64')
license=('GPL2')
makedepends=(
    'git' 'bc' 'kmod' 'python'
    'cpio' 'tar' 'xz'  # IKERNEL_HEADERS
    'clang' 'llvm' 'lld'  # CLANG build
)
url="http://www.kernel.org"
options=(!strip)

validpgpkeys=(
    'ABAF11C65A2970B130ABE3C479BE3E4300411886'  # "Linus Torvalds"
    '647F28654894E3BD457199BE38DBBDC86092693E'  # "Greg Kroah-Hartman"
    'E27E5D8A3403A2EF66873BBCDEA66FF797772CDC'  # "Sasha Levin"
)

source=(
    #"linux-stable::git+https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git#tag=${_tag}?signed"
    "linux-stable::git+https://kernel.googlesource.com/pub/scm/linux/kernel/git/stable/linux?signed#tag=${_tag}"
    # the main kernel config files
    'config-server.x86_64'
    # sysctl config
    'sysctl-linux-server.conf'
)

## extra patches
_extrapatches=(
)
if [[ ${#_extrapatches[@]} -ne 0 ]]; then
    source=( "${source[@]}"
        "${_extrapatches[@]}"
    )
fi

sha512sums=('747934c10036d6032f8cd8c67c16fc59416d6266f13280bc1d9849b006285b311abc0230c280d7441a3c1a42bea689866c1933c484819f951effc16a0eef9f32'
            '56563370defb8115177fcf12c6449199d67d53460db0c2be20059eaf820e6b0591cf2f539e794821bb5f81b01364089f3d40a0a4899987b0dc2cba0e8fc58e11'
            '651e94c285cef48e600cf42a66651c71f3a9c7774b16c54936c35e1485a35382777ef84b3f01bf036c95fa141c941eae33867fd605c91e5f8b47617e0c0ef0c3')

export KBUILD_BUILD_HOST=blackeagle
export KBUILD_BUILD_USER=$pkgbase
export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"
export LOCALVERSION=
export KERNEL_BUILD_FLAGS='LLVM=1 LLVM_IAS=1'  # CLANG
#export KERNEL_BUILD_FLAGS=''  # GCC

prepare() {
    cd "$srcdir/linux-stable"

    # extra patches
    for patch in ${_extrapatches[@]}; do
        patch="$(basename "$patch" | sed -e 's/\.\(gz\|bz2\|xz\)//')"
        pext=${patch##*.}
        if [[ "$pext" == 'patch' ]] || [[ "$pext" == 'diff' ]]; then
            msg2 "apply $patch"
            patch -Np1 -i "$srcdir/$patch"
        fi
    done

    # set configuration
    msg2 "copy configuration"
    cp "$srcdir/config-server.x86_64" ./.config
    if [[ "$_kernelname" != "" ]]; then
        sed -i "s|CONFIG_LOCALVERSION=.*|CONFIG_LOCALVERSION=\"\U$_kernelname\"|g" ./.config
    fi

    # set build salt
    sed -i "s|CONFIG_BUILD_SALT=.*|CONFIG_BUILD_SALT=\"\U$pkgbase-$pkgver-$pkgrel\"|g" ./.config

    # set extraversion to pkgrel
    msg2 "set extraversion to $pkgrel"
    sed -ri "s|^(EXTRAVERSION =).*|\1 -$pkgrel|" Makefile
}

build() {
    cd "$srcdir/linux-stable"

    if [[ -n $KERNEL_CONFIG ]]; then
        msg2 "prepare"
        make $KERNEL_BUILD_FLAGS prepare
        # Configure the kernel. Replace the line below with one of your choice.
        make $KERNEL_BUILD_FLAGS nconfig # preferred CLI menu for configuration
        ##make $KERNEL_BUILD_FLAGS menuconfig # CLI menu for configuration
        return 1
    else
        msg2 "olddefconfig"
        make $KERNEL_BUILD_FLAGS olddefconfig
    fi

    # build!
    make $KERNEL_BUILD_FLAGS -s kernelrelease > version
    msg2 "build"
    make $KERNEL_BUILD_FLAGS $MAKEFLAGS bzImage modules
}

package_linux-besrv-lts() {
    pkgdesc="The Linux Kernel and modules (lts), BlackEagle Server Edition"
    depends=('coreutils' 'kmod>=26' 'initramfs')
    optdepends=(
        'crda: to set the correct wireless channels of your country'
        'linux-firmware: when having some hardware needing special firmware'
    )
    provides=(
        'linux'
        'VIRTUALBOX-GUEST-MODULES'
        'WIREGUARD-MODULE'
        'KSMBD-MODULE'
    )

    cd "$srcdir/linux-stable"
    local kernver="$(<version)"
    local modulesdir="$pkgdir/usr/lib/modules/$kernver"

    msg2 "Installing boot image..."
    # systemd expects to find the kernel here to allow hibernation
    # https://github.com/systemd/systemd/commit/edda44605f06a41fb86b7ab8128dcf99161d2344
    install -Dm644 "$(make $KERNEL_BUILD_FLAGS -s image_name)" "$modulesdir/vmlinuz"

    # Used by initramfs to name the kernel
    echo "$pkgbase" | install -Dm644 /dev/stdin "$modulesdir/pkgbase"

    msg2 "Installing modules..."
    # DEPMOD=/doesnt/exist => suppress depmod
    make $KERNEL_BUILD_FLAGS INSTALL_MOD_STRIP=1 \
        INSTALL_MOD_PATH="$pkgdir/usr" \
        DEPMOD=/doesnt/exist \
        modules_install

    # install sysctl tweaks
    install -Dm644 "$srcdir/sysctl-linux-server.conf" \
        "$pkgdir/usr/lib/sysctl.d/60-linux-besrv-lts.conf"

    # remove build link
    rm "$modulesdir"/build
}

package_linux-besrv-lts-headers() {
    pkgdesc="Header files and scripts for building modules for linux$_kernelname"
    provides=('linux-headers')
    depends=('clang' 'llvm' 'lld')  # make sure CLANG is installed

    KARCH=x86

    cd "$srcdir/linux-stable"
    local kernver="$(<version)"
    local builddir="$pkgdir/usr/lib/modules/$kernver/build"

    msg2 "Installing build files..."
    install -Dt "$builddir" -m644 \
        .config \
        Makefile \
        Module.symvers \
        System.map \
        version
    install -Dt "$builddir/kernel" -m644 kernel/Makefile
    install -Dt "$builddir/arch/x86" -m644 arch/x86/Makefile

    # add objtool for external module building and enabled VALIDATION_STACK option
    install -Dt "$builddir/tools/objtool" tools/objtool/objtool

    if [[ -e "$builddir/tools/bpf/resolve_btfids" ]]; then
        # required when DEBUG_INFO_BTF_MODULES is enabled
        install -Dt "$builddir/tools/bpf/resolve_btfids" \
            tools/bpf/resolve_btfids/resolve_btfids
    fi

    msg2 "Installing headers..."
    find . -path './include/*' -prune -o -path './scripts/*' -prune \
        -o -type f \( -name 'Makefile*' -o -name 'Kconfig*' \
        -o -name 'Kbuild*' -o -name '*.sh' -o -name '*.pl' \
        -o -name '*.lds' \) | bsdcpio -pdm "$builddir"
    cp -t "$builddir" -a scripts include
    find $(find arch/$KARCH -name include -type d -print) -type f \
        | bsdcpio -pdm "$builddir"
    find $(find arch/$KARCH -name kernel -type d -print) -iname '*.s' -type f \
        | bsdcpio -pdm "$builddir"


    # add objtool for external module building and enabled VALIDATION_STACK option
    install -Dt "$builddir/tools/objtool" tools/objtool/objtool

    msg2 "Removing unneeded architectures..."
    local arch
    for arch in "$builddir"/arch/*/; do
        [[ $arch = */$KARCH/ ]] && continue
        echo "Removing $(basename "$arch")"
        rm -r "$arch"
    done

    msg2 "Removing documentation..."
    rm -r "$builddir/Documentation"

    msg2 "Removing broken symlinks..."
    find -L "$builddir" -type l -printf 'Removing %P\n' -delete

    msg2 "Removing loose objects..."
    find "$builddir" -type f -name '*.o' -printf 'Removing %P\n' -delete

    msg2 "Stripping build tools..."
    local file
    while read -rd '' file; do
        case "$(file -bi "$file")" in
            application/x-sharedlib\;*)      # Libraries (.so)
                strip -v $STRIP_SHARED "$file" ;;
            application/x-archive\;*)        # Libraries (.a)
                strip -v $STRIP_STATIC "$file" ;;
            application/x-executable\;*)     # Binaries
                strip -v $STRIP_BINARIES "$file" ;;
            application/x-pie-executable\;*) # Relocatable binaries
                strip -v $STRIP_SHARED "$file" ;;
        esac
    done < <(find "$builddir" -type f -perm -u+x ! -name vmlinux -print0)

    msg2 "Adding symlink..."
    mkdir -p "$pkgdir/usr/src"
    ln -sr "$builddir" "$pkgdir/usr/src/$pkgbase"
}
